from scapy.all import *
dst=input("\nEnter the destination IP address :")

ether=Ether()
ether.dst="00:e0:1c:3c:22:b4"
ether.src="FF:FF:FF:FF:FF:FF"

arp=ARP()
arp.op=1
arp.hwsrc="00:e0:1c:3c:22:b4"
arp.psrc="192.168.1.232"
arp.pdst=dst
arp.hwdst="00:00:00:00:00:00"

p=srp1(ether/arp)
print("The MAC address of the device is : ", p.hwsrc)