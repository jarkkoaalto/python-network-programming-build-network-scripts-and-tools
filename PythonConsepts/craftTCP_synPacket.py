import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *

tcp=TCP()
tcp.flags='S'
tcp.dport=80
ip=IP()
ip.src="192.168.1.232"
ip.dst="192.168.1.247"
send(ip/tcp)