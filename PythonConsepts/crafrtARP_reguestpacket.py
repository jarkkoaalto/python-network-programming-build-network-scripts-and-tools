import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *

ether=Ether()
ether.dst="00:00:00:00:00:00"
ether.src="FF:FF:FF:FF:FF:FF"
arp=ARP()
#  ls(ARP)
arp.op=1
arp.hwsrc="00:00:00:00:00:00"
arp.hwdst="FF:FF:FF:FF:FF:FF"
arp.psrc="192.168.1.232"
arp.pdst="192.168.1.247"
sendp(ether/arp)