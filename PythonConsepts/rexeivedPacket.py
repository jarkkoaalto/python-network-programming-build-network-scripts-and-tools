import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *

icmp=ICMP()
icmp.type=8
icmp.code=0

ip=IP()
ip.src="192.168.1.232"
ip.dst="192.168.1.247"

p=sr1(ip/icmp)
p.show() # display ip(information) and ICMP(information) 
p.ttl # how many packet
