'''
Lab design:

Ubuntu:
-   python 2.7.3
-   Scapy 2.2.0-1
-   Wireshark

Windows 7:
-   Wireshark
-   IIS(Web Server)

Code, Script would be executed from Ubuntu.
Packet verification would be done on Windows7 with wireshark

1. Start WireShark in Windows7
2. Move to Ubuntu and start scapy
3. Run script
4. Open Windows 7 Wireshark and find trafig src->dst 

'''
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *

icmp=ICMP()
icmp.type=8
icmp.code=0

ip=IP()
ip.dst="192.168.1.247"
ip.src="192.168.1.232"
send(ip/icmp,verbose=0)
